<!DOCTYPE HTML>

<html>

<head>
	<meta http-equiv="Content-Type" charset="UTF-8" />
	<title>Shopping exercise</title>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="style.css">
    <script src="vendors/jquery/jquery.js"> </script>

</head>

<body>

	<header>
		<nav>
			<ul>
				<li id="dropdownCart" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><a href="#" >Your shopping cart</a>

					<div class="dropdown-menu" aria-labelledby="dropdownCart">
						<ul class="cart">
							<li class="dropdown-item">Total: <span id="total"> 0 </span> RON</li>
							<li class="dropdown-item"><button id="delete_items" type="button" class="btn btn-default">Remove all items</button></li>
						</ul>
				    </div>
				</li>
			</ul>
		</nav>
	</header>

	<section>

		<article>
            <div class="row">
                <div class="product__grid">

                </div>

	          	<button type="button" class="btn btn-default show-more">Show more</button>


				<div class="modal fade" id="cartModal" role="dialog">
				    <div class="modal-dialog">

				      <!-- Modal content-->
				      <div class="modal-content">
				        <div class="modal-header">
				          <h4 class="modal-title"></h4>
				        </div>
				        <div class="modal-body">
				          <p></p>
				        </div>
				        <div class="modal-footer">
				          <button type="button" class="btn btn-default" data-dismiss="modal">Add to Cart</button>
				        </div>
				      </div>

				    </div>
				  </div>
            </div>
		</article>

	</section>


	<footer>
		<p>Testing</p>
	</footer>

	<script>
		$( document ).ready(function() {
			shopping.Product.getData();

			$(".product__grid").on('click', '.btn-secondary', function(){
				shopping.Product.showMore($(this));
			});

			$(".product__grid").on('click', '.btn-primary', function(){
				shopping.Cart.showCart($(this));
				shopping.Cart.calculateTotal();
			});

			shopping.Cart.eraseTotal();

		});
	</script>


	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"> </script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"> </script>
	<script src="custom.js"> </script>

</body>

</html>
