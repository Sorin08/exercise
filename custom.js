shopping = {
    Product: {
        createGrid: function (name, image, price, currency, ron_price) {
            var html = '';
            html += '<div class="col-sm-4"> <div class="product__card">';
            if (image !== "") {
                html += '<img class="product__image" src="' + image + ' "> ';
            } else {
                html += '<img class="product__image" src=" noimage.png "> ';
            }
            html += '<h2 class="product__name">' + name + '</h2>';
            html += '<p class="product__price">' + price + " " + currency + ' (<span class="ron__price">' + ron_price + '</span> RON)' + '</p>';
            html += '<button type="button" class="btn btn-primary">Add to Cart</button>';
            html += '<button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#cartModal">See more info</button>';
            html += '</div>';
            html += '</div>';
            $(".product__grid").append(html);
        },

        getData: function () {
            $.getJSON( 'take-prods.php', function( json ) {
                var ct = 0;
                for ( ct = 0; ct < 9; ct++) {
                    shopping.Product.createGrid(json[ct].name, json[ct].images, json[ct].price, json[ct].currency, json[ct].ron_price);
                    console.log(ct);
                }
                $('.show-more').click(function () {
                    alert('test');
                    for (ct = 9; ct < ct + 3; ct++) {
                        shopping.Product.createGrid(json[ct].name, json[ct].images, json[ct].price, json[ct].currency, json[ct].ron_price);
                        console.log(ct);
                    }
                });

            })

        },
        showMore: function (id) {
            var title = id.parent().find('.product__name').html();
            console.log(title);
            $('.modal-title').append(title);

            $('#cartModal').on('hidden.bs.modal', function (e) {
                $(".modal-title").empty();
          });
        }
    },

    Cart: {
        showCart: function (id) {
            var title = id.parent().find('.product__name').html();
            var price = id.parent().find('.ron__price').html();

            var html = '';
            html += '<li class="dropdown-item name">' + title + ' - <span class="cart__price">' + price + ' RON </span></li>';

            $(".cart").prepend(html);
        },
        calculateTotal: function () {
            var total = parseFloat($('#total').html());
            var price = parseFloat($('.cart__price').html());

            total = total + price;

            $("#total").html('').append(total);
        },
        eraseTotal: function () {
            $('#delete_items').on('click', function(){
                $('.dropdown-item.name').remove();
                $("#total").html('0');
            });
        }
    }
}
